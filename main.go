package main

import (
	"fmt"

	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/kdsingh333/Golang-Todo/database"
	"github.com/kdsingh333/Golang-Todo/models"
	"github.com/kdsingh333/Golang-Todo/router"
)

func main() {

	engine := gin.Default()

	router.Router(engine);
	ok :=godotenv.Load();
	if ok!=nil{
		fmt.Print("Error during loading env file");
	}
	DNS := os.Getenv("DNS");
	
	DbVar,err :=database.GetDB(DNS);
	if err!= nil{
		fmt.Printf("Error detected during connecting database %v",err);
	}
	
	models.Db = DbVar;
	models.Db.AutoMigrate(&models.Task{})

	engine.Run(":5000");
}
