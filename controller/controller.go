package controller

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/kdsingh333/Golang-Todo/models"
)

func AddTask(c *gin.Context) {
	   var body AddTaskRequest
       if err:=c.BindJSON(&body); err!= nil{
           fmt.Print("The error occure during binding")
	   }
        
	   ok:=models.AddTask(body.Task)
	   if ok!=nil{
		fmt.Printf("Error detected during Adding task")
	   }
}

func GetTask(c *gin.Context) {
       user,err :=models.GetTask();
	   if err!=nil{
		fmt.Print("Error in getting task")
	   }
	   c.JSON(http.StatusOK,user);
}

func DeleteTask(c *gin.Context) {
	var body DeleteTaskRequest 

	if err:=c.BindJSON(&body); err!= nil{
		fmt.Print("The error occure during binding")
	}
	ok:=models.DeleteTask(body.DeleteTask)
	   if ok!=nil{
		fmt.Printf("Error detected during Adding task")
	   }

}

func UpdateTask(c *gin.Context) {
    var body UpdateTaskRequest

	if err:= c.BindJSON(&body); err!= nil{
		fmt.Print("The error occure during binding")
	}

	ok:= models.UpdateTask(body.Task,body.UpdateTask)
	   if ok!=nil{
		fmt.Printf("Error detected during Updating task")
	   }
}