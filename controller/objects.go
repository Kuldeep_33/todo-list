package controller


type (
	AddTaskRequest  struct{
		Task string `json:"task,omitempty"`
	}
	UpdateTaskRequest struct{
		Task       string `json:"task,omitempty"`
		UpdateTask string `json:"update_task,omitempty"`
	}
	DeleteTaskRequest struct{
		DeleteTask string `json:"delete_task,omitempty"`
	}
   
) 

