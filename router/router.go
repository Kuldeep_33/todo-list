package router

import (
	"github.com/gin-gonic/gin"
	"github.com/kdsingh333/Golang-Todo/controller"
)

func Router(r *gin.Engine) {
	r.POST("/Add", controller.AddTask)
	r.GET("/Task", controller.GetTask)
	r.DELETE("/Delete", controller.DeleteTask)
	r.PUT("/Update", controller.UpdateTask)
}
