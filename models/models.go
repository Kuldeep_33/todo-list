package models

import "gorm.io/gorm"

var Db *gorm.DB

type Task struct {
	ID   int    `json:"id,omitempty"`
	Task string `json:"task,omitempty"`
}
type GetAllTaskRequeste struct {
	Task string `json:"task,omitempty"`
}

func AddTask(task string) error {
	err := Db.Create(&Task{Task: task}).Error
	if err != nil {
		return err
	}
	return err

}
func UpdateTask(task, update_task string) error {
	err := Db.Model(&Task{}).Where(Task{Task: task}).Updates(&Task{Task: update_task}).Error
	if err != nil {
		return err
	}
	return err
}
func DeleteTask(task string) error {
	ok := Db.Where("task = ?", task).Delete(&Task{}).Error
	if ok != nil {
		return ok
	}

	return nil

}
func GetTask() ([]GetAllTaskRequeste, error) {
	var body []GetAllTaskRequeste

	ok := Db.Model(&Task{}).Find(&body)
	if ok.Error != nil {
		return nil, ok.Error
	}

	return body, nil

}
