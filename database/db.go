package database

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)
func GetDB(DNS string) (*gorm.DB,error){  
   db,err := gorm.Open(postgres.Open(DNS),&gorm.Config{});
   if err!=nil{
	return nil,err
   }
   return db,nil
}